import React from 'react';
import { Input } from 'semantic-ui-react'

const userinput = ({change, username}) =>
  <Input
    className="UserInput"
    type="text"
    onChange={change}
    value={username}
    icon="pencil"
  />

export default userinput
