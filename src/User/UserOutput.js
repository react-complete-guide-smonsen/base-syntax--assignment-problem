import React from 'react';
import { Segment } from 'semantic-ui-react'

const useroutput = ({username}) =>
  <div id="user-posts">
    <h2>Posts for <span style={{color: 'grey'}}>{ username }</span></h2>
    <Segment><span className="username">{username}:</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate alias delectus aperiam, consectetur. Nobis minus dolorum unde accusantium quam, repellat voluptatibus velit. Earum, veniam ratione facilis reiciendis animi, voluptatum laboriosam.</Segment>
    <Segment><span className="username">{username}:</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate alias delectus aperiam, consectetur. Nobis minus dolorum unde accusantium quam, repellat voluptatibus velit. Earum, veniam ratione facilis reiciendis animi, voluptatum laboriosam.</Segment>
  </div>

export default useroutput
