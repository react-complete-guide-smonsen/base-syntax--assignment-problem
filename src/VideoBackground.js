import React, { Fragment } from 'react'
import './VideoBackground.css'

const VideoBackground = ({videoId}) =>
  <Fragment>
    <div className="video-background">
      <div className="video-foreground">
        <iframe title="Hack The Planet" src={"https://www.youtube.com/embed/" + videoId + "?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=W0LHTWG-UmQ"} frameBorder="0" allowFullScreen></iframe>
      </div>
    </div>
    <div id="overlay"></div>
  </Fragment>

export default VideoBackground
