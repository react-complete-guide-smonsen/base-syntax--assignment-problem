import React from 'react'
import { Button, Form, Grid, Header, Image, Message, Segment, Icon } from 'semantic-ui-react'

const LoginForm = props => (
  <div className='login-form' id="login-form">
    {/*
      Heads up! The styles below are necessary for the correct render of this example.
      You can do same with CSS, the main idea is that all the elements up to the `Grid`
      below must have a height of 100%.
    */}
    <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>
    <Grid
      textAlign='center'
      style={{ height: '100%' }}
      verticalAlign='top'
    >
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' textAlign='center'>
          <Image style={{width: '50%'}} src='/logo.png' />
          <br />
          <span className="login-prompt" style={{padding: '0.5rem 1rem'}}>Log-in to <i><b style={{textDecoration: 'underline'}}>your</b></i> account xD</span>
        </Header>
        <Form size='large' onSubmit={e => props.submit(e)}>
          <Segment stacked>
            <Form.Input
              fluid
              icon='user'
              iconPosition='left'
              name='username'
              placeholder='Username'
              onChange={e => props.handleUsernameInput(e.target.value)}
            />
            <Form.Input
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              name='password'
              type='password'
              onChange={e => props.handlePasswordInput(e.target.value)}
            />

            <Button color='grey' fluid size='large'>Log-in</Button>
          </Segment>
        </Form>
        <Message>
          <Icon name='info circle'/><i><b>Hint:</b> Hack in by reading the <a href="https://gitlab.com/react-complete-guide-smonsen/base-syntax--assignment-problem/blob/master/src/App.js" target="_blank" rel="noopener noreferrer">source code</a> or <a href="https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en" target="_blank" rel="noopener noreferrer">React Devtools</a>.</i>
        </Message>
      </Grid.Column>
    </Grid>
  </div>
)

export default LoginForm
