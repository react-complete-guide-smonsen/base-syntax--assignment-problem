import React, { Component, Fragment } from 'react';
import { Button, Image } from 'semantic-ui-react'
import _ from 'lodash'
import 'semantic-ui-css/semantic.min.css';
import './App.css';
import UserInput from './User/UserInput';
// import Modal from './Modal';
import LoginForm from './LoginForm';
import VideoBackground from './VideoBackground';
import UserOutput from './User/UserOutput';

class App extends Component {
  state = {
    username: 'Anonymous',
    users: [
      { username: 'admin', password: 'g0d' },
      { username: 'Raz0r', password: '~~~bLaDeSuXxX~~~' },
      { username: 'BladE', password: '0MgWTfbBq!' },
      { username: 'sfalken', password: 'j0shEua1981' },
      { username: 'zer0c00l', password: '^(%r^$346%$3)' },
      { username: 'max', password: 'r3actR0x!' },
      { username: 'smonsen', password: '1337HxAxxoRr' },
    ],
    postsVisible: false,
    authenticated: false,
    messages: {
      flash: ''
    }
  }

  usernameInput;
  passwordInput;
  delay = 2000;

  handleUsernameInput = input => this.usernameInput = input;
  handlePasswordInput = input => this.passwordInput = input;

  setFlash = msg =>
    this.setState(
      () => ({messages: {flash: msg}}),
      () => setTimeout(() => this.setState({messages: {flash: ''}}), this.delay)
    )

  updateUsername = username => {
    this.setState(
      state => {
        const { users } = state;
        const index = _.findIndex(users, user => user.username === state.username);
        users[index].username = username;
        return { users, username }
      }
    )
  }

  // Destructuring 'event.target.value' to 'username' local arg
  handleUserInput = ({ target: { value: username } }) =>
    this.setState(
      () => ({ username }), // ES6 shorthand for { username: username }
      this.updateUsername(username)
    )

  toggleViewPosts = () => this.setState(state => ({postsVisible: !state.postsVisible}))

  authenticate = (e) => {
    e.preventDefault()

    const { users } = this.state

    const username = this.usernameInput;
    const password = this.passwordInput;

    // Return first user w/ matching credentials
    const match = users.find(user => (user.username === username && user.password === password))

    // Functional setState w/ callback: https://reactjs.org/docs/react-component.html#setstate
    match ?
      this.setState(
        () => ({authenticated: true, username}),
        this.setFlash('Login Success!'),
      )
    :
      this.setState(
        () => ({authenticated: false}),
        this.setFlash('Login Failed!')
      )
  }

  logout = () =>
    this.setState(
      () => ({authenticated: false}),
      this.setFlash('Logged Out!'),
    )

  // componentDidUpdate() {
  //   console.log(this.state.users);
  // }

  render() {
    const { authenticated, messages, postsVisible } = this.state
    return (
      <div className="App">
        <VideoBackground videoId={ !authenticated ? "W0LHTWG-UmQ" : "ITfQGEASYvU" } />

        { messages.flash && <p className="flash-message">{messages.flash}</p> }

        { authenticated &&
          <Fragment>
            <div id="login-area">
              <UserInput change={this.handleUserInput} username={this.state.username} />
              <Button inverted={!postsVisible} className="toggleViewPosts" onClick={this.toggleViewPosts}>{postsVisible ? 'Hide Posts' : 'View Posts'}</Button>
              <Button inverted onClick={this.logout}>Log out</Button>
            </div>
            { postsVisible && <UserOutput username={this.state.username} /> }
            <Image style={{width: '13%'}} className="positioned-logo" src='/logo.png' />
          </Fragment>
        }

        { !authenticated &&
          <LoginForm
            submit={this.authenticate}
            handleUsernameInput={this.handleUsernameInput}
            handlePasswordInput={this.handlePasswordInput}
          />
        }
      </div>
    )
  }
}

export default App;
