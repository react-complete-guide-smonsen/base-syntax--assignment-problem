## Course Assignment
Section 3: [base-syntax--assignment-problem](https://www.udemy.com/react-the-complete-guide-incl-redux/learn/v4/t/practice/16064/introduction)

## Setup

```console
npm i && npm start
```

Then read the source code of [&lt;App/&gt;](https://gitlab.com/react-complete-guide-smonsen/base-syntax--assignment-problem/blob/master/src/App.js) or [React Devtools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) to hack in to the system with a username and password.

_(Note: There's a surprise waiting in the account dashboard once you're successful.)_

### **And remember...**
#### \~\~\~Hacking isn't just a crime. _It's a survival trait._~~~

![Screen shot](https://bliskcloudstorage.blob.core.windows.net/screenshots/8f05b349-d3d5-4836-9767-1679d059cff8%5Cf815c22e-d297-40be-b89c-005b8871e3ba.png)

## Student Notes
I'm an intermediate React developer, so I took the liberty to tryout some of the new features on React 16 and incorporate some other npm modules like:

- [React Semantic-UI](https://react.semantic-ui.com) which is an official port of _Semantic UI_ to React.
- [lodash](https://lodash.com/docs/) - which is a near ubiquitous library for React and other development. Used [.findIndex()](https://lodash.com/docs/4.17.4#findIndex) to update the current user's username when changed, from an array of other users in `state.users` in `App.updateUsername()`.
- Used the new [functional setState](https://reactjs.org/docs/react-component.html#setstate) w/ optional callback for setting a flash message.
- Used [nested named destructuring](https://gitlab.com/react-complete-guide-smonsen/base-syntax--assignment-problem/blob/master/src/App.js#L55-60) pattern to turn `event.target.value` into `username` local method variable. See: [MDN: Nested object and array destructuring](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment#Nested_object_and_array_destructuring)

```javascript
handleUserInput = ({ target: { value: username } }) =>
  this.setState(
    () => ({ username }), // ES6 shorthand for { username: username }
    this.updateUsername(username)
  )
```

- Video background component using [this pen](https://codepen.io/dudleystorey/pen/PZyMrd) by [Dudley Storey](https://codepen.io/dudleystorey)
- [Fragments](https://reactjs.org/blog/2017/09/26/react-v16.0.html#new-render-return-types-fragments-and-strings) (React 16)
